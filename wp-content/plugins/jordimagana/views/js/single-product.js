jQuery(document).on('ready',function(){
		calcularMaximo();
		jQuery(document).on('woocommerce-product-addons-update',function(){
			var inp = jQuery('input[name="addon-2289-envas-de-retorn-0"]');
			if(typeof inp.length != 'undefined'){
				var val = parseFloat(inp.val());
				var max = inp.attr('max');
				if(!isNaN(val) && val > 0 && typeof(max)!='undefined'){
					var max = parseFloat(max);
					if(val>max){
						jQuery('input[name="addon-2289-envas-de-retorn-0"]').val(max);
						jQuery('form.cart').trigger('woocommerce-product-addons-update');
					}
				}
				if(val < 0){
					jQuery('input[name="addon-2289-envas-de-retorn-0"]').val(0);
					jQuery('form.cart').trigger('woocommerce-product-addons-update');
				}
			}
		});

		jQuery('.qty').on('change',function(){
			calcularMaximo();
		});
	});	

	function calcularMaximo(){
		var amounts = jQuery(jQuery('p.price')[0]).find('.woocommerce-Price-amount');
		if(amounts.length>1){
			amounts = jQuery(amounts[1]);
		}else{
			amounts = jQuery(amounts[0]);
		}
		var price = parseFloat(amounts.text().replace('€','').replace(',','.'));
		var retorno = jQuery('input[name="addon-2289-envas-de-retorn-0"]');
		var cantidad = parseInt(jQuery('.qty').val());
		var priceretorno = 0;
		if(retorno.length>0){
			priceretorno = parseFloat(retorno.data('raw-price'));
		}
		if(priceretorno<0){
			priceretorno*=-1;
		}
		if(priceretorno>0){
			var max = Math.floor((price*cantidad)/priceretorno);
			jQuery('input[name="addon-2289-envas-de-retorn-0"]').attr('max',max);
		}
	}