<div data-grid-overlay="black" class="vc_row wpb_row vc_row-fluid vc_custom_1581067775555 vc_row-has-fill vc_column-gap-40 vc_row-o-equal-height vc_row-o-content-middle vc_row-flex vc-row-4628 grid-overlay-active">
	<div class="container et-clearfix">
		<div class="text768-1023-align-center text767-align-center wpb_column vc_column_container vc_col-sm-12 vc_col-lg-1 vc_col-md-2 vc_col-xs-12 text-align-right vc-column-893788">
			<div class="vc_column-inner ">
				<div class="wpb_wrapper">
					<div class="et-icon large et-icon-741124">
						<span class="el-icon far fa-envelope-open"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-5 vc_col-md-5 vc_col-xs-12 text-align-none vc-column-843886">
			<div class="vc_column-inner ">
				<div class="wpb_wrapper">
					<span class="et-gap et-clearfix et-gap-186183 hide1024"></span>
					<h4 class="et-heading text-align-left tablet-text-align-inherit mobile-text-align-inherit animate-false no-animation-type text768-1023-align-center text767-align-center" id="et-heading-608454" data-delay="0" data-mobile-font="inherit" data-mobile-line-height="inherit" data-tablet-landscape-font="inherit" data-tablet-portrait-font="inherit" data-tablet-landscape-line-height="inherit" data-tablet-portrait-line-height="inherit" data-parallax="false" data-move="false" data-coordinatex="0" data-coordinatey="0" data-speed="10">
					<span class="text-wrapper">
						<span class="text">Consulta la disponibilitat d'entrega</span>
					</span>
					</h4>
				</div>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-5 vc_col-xs-12 text-align-none vc-column-731652">
			<div class="vc_column-inner " data-1024-1280-right="10">
				<div class="wpb_wrapper">
					<span class="et-gap et-clearfix et-gap-322948 hide1024"></span>
					<div id="et-mailchimp-841305" class="et-mailchimp widget_mailchimp">
						<div class="mailchimp-form">
							<form class="et-mailchimp-form" name="et-mailchimp-form" action="" method="POST" onsubmit="searchZipCode(this); return false;">
								<div>
									<input type="text" value="" class="field" name="cpzipcode" placeholder="Codi Postal de l'entrega">
									<span class="alert warning">Invalid or empty email</span>
								</div>
								<div class="send-div">
									<input type="submit" class="button" value="Subscribe" name="subscribe">
									<div class="sending"></div>
								</div>
								<div class="et-mailchimp-success alert final success">You have successfully subscribed to the newsletter.</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="container et-clearfix responseZipData" style="margin-top: 40px;"></div>
	<div class="grid-overlay"></div>
</div>

<script>
	function searchZipCode(f){
		info('.responseZipData','Buscando por favor espere');
		jQuery.ajax({
			url:'/?jm-ajax=1&act=searchzipcode&pr='+jQuery('input[name="cpzipcode"]').val(),
			success:function(data){
				jQuery('.responseZipData').removeAttr('alert alert-info');
				jQuery('.responseZipData').html(data);
			}
		});		
	}
</script>