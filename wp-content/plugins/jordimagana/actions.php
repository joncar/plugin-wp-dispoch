<?php
/*************************************** PRESUPUESTOS ********************************/
//ACTIONS
add_action('init',function(){
	if(!empty($_GET['jm-ajax'])){
		do_action('jm_core');
		die();
	}
});

add_action('jm_core',function(){
	if(!empty($_GET['act'])){
		if(has_action('jm_'.$_GET['act'])){
			$pr = !empty($_GET['pr'])?$_GET['pr']:null;
			do_action('jm_'.$_GET['act'],$pr);
		}
	}else{
		do_action('jm_default');
	}
});

add_action('jm_default',function($limit){		
	return;
});

add_action('jm_searchzipcode',function($pr){		
	global $jckwds;		
	if(is_numeric($pr)){
		$zones = WC_Shipping_Zones::get_zones();
		$zone = 'No encontrada';
		$zoneid = 0;
		foreach($zones as $z){
			$locations = $z['zone_locations'];
			foreach($locations as $l){				
				if($l->code == $pr){
					$zone = $z['zone_name'];
					$zoneid = $z['id'];
				}
			}
		}	
		if($zoneid>0){	
			$day_fees = $jckwds->get_upcoming_bookable_dates();	
			$dias = [];
			foreach($day_fees as $d){
				$timeslots = $jckwds->slots_available_on_date($d['ymd']);
				if($timeslots){
					foreach($timeslots as $t){					
						if(count($dias)<7){
							$dias[] = $d['header_formatted'].'<br>'.$t['timefrom']['time'].'-'.$t['timeto']['time'];
						}
					}			
				}
			}			
		}else{
			$dias[] = 'no hi ha disponibilitat per aquest codi postal';
		}
		require_once JONCARPATH.'/views/form-dates-from-zipcode-ajaxresponse.php';
	}
});

add_action('iconic_wds_before_checkout_fields',function(){ob_start();});
add_action('iconic_wds_after_delivery_details_title',function(){
	ob_end_clean();
	echo '<div id="jckwds-fields" class="iconic-wds-fields woocommerce-billing-fields">';
});

add_action('showCalendars',function(){
	require_once JONCARPATH.'/woocommerce/checkout/order-calendars.php';
});