<?php
/*
Plugin Name: JordiMagana
Plugin URI: https://jordimagana.com
Description: Custom plugin to this website
Version: 1
Author: Jonathan Cardozo
Author URI: https://zerobscrm.com
Text Domain: jordimagana
*/
define('JONCARPATH',dirname(__FILE__));
require_once JONCARPATH.'/actions.php';
require_once JONCARPATH.'/filters.php';
require_once JONCARPATH.'/helpers.php';
require_once JONCARPATH.'/shortcodes.php';

function front_scripts($hook){		
    wp_register_script('single-product', site_url().'/wp-content/plugins/jordimagana/views/js/single-product.js', array('jquery'), '2.3', true,999);
    wp_enqueue_script('single-product');    

    wp_register_script('general', site_url().'/wp-content/plugins/jordimagana/views/js/general.js', array('jquery'), '2.3', true,999);
    wp_enqueue_script('general');    

    wp_register_style('jordimaganaStyle', site_url().'/wp-content/plugins/jordimagana/views/css/style.css');
    wp_enqueue_style('jordimaganaStyle');    
}
add_action("wp_enqueue_scripts", "front_scripts");