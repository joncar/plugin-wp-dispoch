<style>
	input[type="button"].enviosBtn:hover,input[type="button"].enviosBtn.active{
		color: #ffffff !important;
		background-color: #212121 !important;
	}

	input[type="button"].enviosBtn{
		color: #000000;		
		background-color: #fff;
		border: 1px solid #000;
	}

	.wc_payment_methods label{
		display: block;
		margin-bottom: 8px;
		font-size: 16px;
		line-height: 28px;
		text-align: left;
	}
	#jckwds-delivery-date-description, 
	#jckwds-delivery-time-description,
	#pickup-items-field-for-0,
	.pickup-location-field-label{
		display: none !important;
	}

	.woocommerce-shipping-fields,
	.woocommerce-additional-fields{
		display: none;
	}
</style>
<input id="btnDomicili" type="button" class="button alt yith-wcms-button enviosBtn" value="Enviament a domicili" onclick="domicilio()">
<input id="btnLocal" type="button" class="button alt yith-wcms-button enviosBtn" value="Recollida en botiga" onclick="local()">
<div style="margin-top:20px"></div>
<div id="envioADomicilio" class="shipmethod" style="display: none">
	<h4>Sis plau, tria una data d'entrega i horari</h4>
	<?php do_action( 'woocommerce_checkout_order_review' ); ?> 
	<p style="color: #c90c0f;">Despeses  d'enviament 2€</p>
	<div style="margin-top:70px"></div>    
</div>
<div id="envioALocal" class="shipmethod" style="display: none">	
	<h4>Sis Plau, Tria Una Data recollida</h4>
	<?php wc_cart_totals_shipping_html(); ?>	
</div>

<?php 
do_action( 'woocommerce_before_checkout_shipping_form', $checkout );
do_action( 'woocommerce_checkout_shipping' );
do_action( 'woocommerce_checkout_after_customer_details' );
?>

<script>
	jQuery(document).on('ready',function(){
		jQuery('.pickup-location-schedule').before(jQuery("#pickup-location-field-for-0").get());
		jQuery('#pickup-location-field-for-0').prepend('<div style="font-weight: bold;">Adreça de recollida:</div>');
		jQuery('#wc-local-pickup-plus-datepicker-0').before('<div><label style="padding: 0;">Data de recollida:</label></div>');
		jQuery(document).on('change','#jckwds-delivery-date,.pickup-location-appointment-date',function(){		
			jQuery('#fechaEntrega').html(jQuery(this).val());
		});
		jQuery(document).on('change','#jckwds-delivery-time',function(){		
			jQuery('#horaEntrega').html(jQuery(this).find('option:selected').html());
		});
		jQuery(document).on('change','#order_comments',function(){
			jQuery('#notasEntrega').html(jQuery(this).val());
		});
		jQuery('#notasEntrega').html(jQuery('#order_comments').val());
	});

	function local(){
		jQuery('.shipmethod').hide();
		jQuery('#envioALocal').show();
		jQuery('#shipping_method_0_local_pickup_plus').prop('checked',true).trigger('change');
		jQuery('.enviosBtn').removeClass('active');
		jQuery('#btnLocal').addClass('active');
		jQuery('.woocommerce-shipping-fields').hide();
		jQuery('.woocommerce-additional-fields').show();
		jQuery('#fechaEntrega').html(jQuery('wc-local-pickup-plus-datepicker-0').val());
		jQuery('#horaEntrega').html('De 9:00 am a 5:00 pm');
	}

	function domicilio(){
		jQuery('.shipmethod').hide();
		jQuery('#envioADomicilio').show();
		jQuery('#shipping_method_0_free_shipping12').prop('checked',true).trigger('change'); 
		jQuery('.enviosBtn').removeClass('active'); 
		jQuery('#btnDomicili').addClass('active');
		jQuery('.woocommerce-shipping-fields').show();
		jQuery('.woocommerce-additional-fields').show();
		jQuery('#fechaEntrega').html(jQuery('#jckwds-delivery-date').val());
		jQuery('#horaEntrega').html('');
	}
</script>