<?php 

add_filter('iconic_wds_chosen_method',function($default){
	global $jckwds;	
	if(!empty($_GET['isAlready'])){
		return $_GET['isAlready'];
	}
	if(!empty($_GET['pr']) && $_GET['act'] == 'searchzipcode'){
		$pr = $_GET['pr'];
		$zones = WC_Shipping_Zones::get_zones();
		$zone = 0;
		foreach($zones as $z){
			$locations = $z['zone_locations'];
			foreach($locations as $l){
				if($l->code == $pr){
					$zone = $z['id'];
				}
			}
		}
		if($zone!=0){
			//$jckwds->get_shipping_method_options()
			$disponibles = $jckwds->get_shipping_method_options();
			foreach($disponibles as $n=>$d){
				list($type,$id) = explode(':',$n);
				if($id==$zone){
					$_GET['isAlready'] = $n;
					return $n;
				}
			}
		}
	}
	return $default;
});


add_filter('yith_wcms_template_path_checkout_form',function(){
	return JONCARPATH .'/woocommerce/checkout/form-checkout.php';	
});
add_filter( 'woocommerce_locate_template',function($template, $template_name, $template_path){	  
	  global $woocommerce;
	  $_template = $template;
	  if ( ! $template_path ) $template_path = $woocommerce->template_url;
	  $plugin_path  = JONCARPATH . '/woocommerce/';
	  // Look within passed path within the theme - this is priority
	  $template = locate_template(
	    array(
	      $template_path . $template_name,
	      $template_name
	    )
	  );
	  //echo $plugin_path . $template_name;

	  // Modification: Get the template from this plugin, if it exists	  
	  if ( ! $template && file_exists( $plugin_path . $template_name ) )
	    $template = $plugin_path . $template_name;
	  // Use default template
	  if ( ! $template )
	    $template = $_template;
	  // Return what we found
	  return apply_filters('jordimagana_locate_template',$template);
}, 10, 3 );